# ISI TP3 Crypto / Severin Matthias - Nguyen Kevin

## Question 1

Les deux clé USB contiennent un fichier password qui contient une clé de chiffrage et qui est protégé par un mot de passe.
Il faut décrypter le fichier avec les deux clés dans l'ordre pour obtenir le fichier qui contient les informations des cartes bancaires.

## Question 2

### Initialisation :

On crée 4 dossiers pour représenter le systeme :
- USB1
- USB2
- DISK
- RAMDISK

On génére les deux clé dans USB1 et USB2 :
dd if=/dev/random bs=32 count=1 | hexdump -ve '1/1 "%02x"' > USB1/password
dd if=/dev/random bs=32 count=1 | hexdump -ve '1/1 "%02x"' > USB2/password

On crée les mots de passe qui protegent les fichier password :
gpg -c USB1/password
gpg -c USB2/password

Ca nous crée les fichiers protégés password.gpg, on peut donc supprimer les fichiers password :
rm USB1/password
rm USB2/password

On crée un fichier DISK/database, qui contiendra les paires nom / carte bancaires cryptées.
touch DISK/database

### 1.i Mise en service

On decrypte les clés avec les mots de passes des deux personnes :
gpg USB1/password.gpg
gpg USB2/password.gpg

Puis on décrypte database.crypt dans la RAM :
./decryptV1


### 1.ii Ajouter une paire

./addpair.sh nom numero_carte

### 1.iii Retirer une paire

./deletepair.sh nom numero_carte

### 1.iv Chercher les numéros de cartes associées à un nom

./searchpair.sh nom

### Crypter la database dans le disque

./cryptV1

Puis supprimer les fichier password des clés USB.

## Question 3

Nous avons décidé de donner la même clé aux représentants qu'aux responsables. 
Mais chacun aura une clé USB différente et le fichier password sera protégé par un mot de passe différent.

Nous avons fait le choix de clé identique car si le représentant et le responsable ont des clés diffèrentes mais permettant toute les 2 de faire le décryptage, il suffit d'avoir l'une des 2 clés pour faire décrypter, c'est pourquoi les clés pour le représentant ou le responsable qu'elle soit identique ou non ne change rien a la sécurité de notre système. (pour le cadre de ce TP uniquement)
## Question 4

Il faut modifier le systeme de cryptage / décryptage.

Pour crypter :
./cryptV2 USB_technique USB_juridique

Pour décrypter :
./decryptV2 USB_technique USB_juridique

## Question 5

Pour la répudiation, on va créer une nouvelle clé et la donner aux nouveaux responsables et représentants.
C'est avec cette nouvelle clé que la database sera cryptée, comme ca la personne qui est parti garde sa clé mais elle devient inutilisable.

## Question 6

Il faut d'abord décrypter la database.

Ensuite il faut lancer la commande :
./repudiation USB_responsable USB_representant

Avec la clé USB des nouveaux responsable et représentant.

On crée un mot de passe pour les nouvelles clés.

gpg -c USB_responsable/password
gpg -c USB_representant/password

Il faut maintenant recrypter la database avec la nouvelle clé
