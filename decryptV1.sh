#!/bin/bash

openssl enc -d -aes-256-ecb -in DISK/database.crypt -out RAMDISK/databasetmp -K $(cat USB2/password)
openssl enc -d -aes-256-ecb -in RAMDISK/databasetmp -out RAMDISK/database -K $(cat USB1/password)
rm RAMDISK/databasetmp
