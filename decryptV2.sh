#!/bin/bash

key1=$(cat $1/password)
key2=$(cat $2/password)

openssl enc -d -aes-256-ecb -in DISK/database.crypt -out RAMDISK/databasetmp -K $key2
openssl enc -d -aes-256-ecb -in RAMDISK/databasetmp -out RAMDISK/database -K $key1
rm RAMDISK/databasetmp
